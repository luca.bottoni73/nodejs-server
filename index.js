const express = require('express');
const moment = require('moment');
require('dotenv').config();
const app = express();

const config = {
    PORT: process.env.PORT,
    ENV: process.env.ENV,
    STATIC: process.env.STATIC,
    LABEL: (req) => `${req.method} ${req.url} request timing`
}
app.use(express.static('public'));//static file direct //http://localhost:3000/public/file.txt
app.use('/static', express.static(__dirname + config.STATIC));//http://localhost:3000/static/file.txt

const InterceptorSend = function (req, res, next) {
    var oldSend = res.send;

    res.send = function (data) {
        oldSend.apply(res, arguments);
        console.timeEnd(config.LABEL(req));
    }
    next();
}
app.use(InterceptorSend);
app.use(function (req, res, next) {
    console.time(config.LABEL(req));
    next();
});

app.get('/', (req, res) => {
    res.send('Hello World!')
})


app.get('/example',
    function (req, res, next) {
        console.log('Step 1');
        next();
    },
    function (req, res, next) {
        console.log('Step 2');
        res.send('Hello from example 2');
    },
    function (req, res, next) {
        console.log('Step 3');
        res.send('Hello from example 2B');
    }
)

app.post('/example', function (req, res, next) {
    console.log('this is example 1-B');
    throw new Error("hello ERROR");// next("Other error!")
    res.send('Hello from example 1-B!');
})

// ERROR HANDLER
app.use(function(err, req, res, next) {
    console.error(err.stack);
    res.status(500).send('Something broke!');
});

// 404 MUST BE LAST MIDDLEWARE
app.use(function(req, res, next) {
    res.status(404);
    res.send("404!!!!");
});

app.listen(config.PORT, () => {
    console.log(`\n**************************************************`);
    console.log(`\t\t CONFIGURATION`);
    Object.entries(config).map(([configuration, value], index) => {
            console.log(` ${configuration.toUpperCase()}: ${value}`);
        }
    )
    console.log(`**************************************************`);
})

